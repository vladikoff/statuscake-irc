var http = require('http');
var fs = require('fs');
var irc = require('irc');
var bot = new irc.Client('irc.mozilla.org', 'fxa-dev-status', {
  debug: true,
  port: 6667,
  channels: ['#fxa-status']
});

var restify = require('restify');

var server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

server.get('/echo', function (req, res, next) {
  console.log(req);
  res.send(req.params);
  return next();
});

server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});